O impacto da aplicação na rotina dos desenvolvedores envolvidos no projeto foi terminantemente decisivo na elaboração da pauta, sendo a utilidade traço fundamental. É um aplicativo que vai atender às nossas demandas dentro e fora da disciplina de Programação de Dispositivos Móveis. 
Pensando nesse contexto de desenvolvimento, ou seja, algo que pudesse ser utilizado pelos integrantes do grupo, buscamos criar uma aplicação simples, mas que fosse útil em nosso cotidiano.

O framework Ionic serve de base para a aplicação de “Lista de Tarefas”, bem como o seu desenvolvimento permite, em contrapartida, geração de conhecimentos e conceitos úteis para a utilização da ferramenta.

Cabe ressaltar que a licença adotada para o código foi de código aberto, denominada GNU General Public License v3.0 (GNU GPLv3), que foi escolhida por meio do link: https://choosealicense.com/. Mais informações sobre a escolha podem ser encontradas no arquivo LICENSE.

Dentre as funções básicas desta aplicação, denominada “Lista de Tarefas”, estão:

        ●	Tela inicial com a acesso a um “menu” para criação de tarefas, que ao ser iniciada emite uma mensagem para que você crie a sua primeira tarefa;
        ●	Criar cards de tarefa com título, data e descrição;
        ●	Visualização dos cards criados por meio da Tela inicial;
        ●	Excluir cards de tarefa, a partir dos valores já criados.

Portanto, as funções disponibilizadas na aplicação são basicamente um CRUD para as tarefas, ou seja, podemos criar, buscar, editar e remover os dados. É um aplicativo bastante útil e simples, por se assemelhar a uma agenda. O fator utilidade foi crucial para optarmos pela criação do mesmo.


