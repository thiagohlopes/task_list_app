import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Task } from 'src/model/task.model';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.page.html',
  styleUrls: ['./task-list.page.scss'],
  providers:[TaskService]
})
export class TaskListPage implements OnInit {
  public taskList: Task[] = []

  constructor(
    public navCtrl: NavController,
    public taskService: TaskService,
  ) { }

  ngOnInit() {
    this.taskService.getAllTask().then((result)=>{
      this.taskList = result     
    })
  }
  ionViewDidEnter() {
    this.taskService.getAllTask().then((result)=>{
      this.taskList = result 
    })
  }

  goBack(){
    this.navCtrl.pop()
  }

  removeTask(id, index){    
    this.taskService.deleteTask(id)
    this.taskList.splice(index, 1)
  }

  goToNewTask(){
    this.navCtrl.navigateForward('/new-task')
  }

}
