import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Task } from 'src/model/task.model';
import { TaskService } from '../task.service';


@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.page.html',
  styleUrls: ['./new-task.page.scss'],
  providers:[TaskService]
})
export class NewTaskPage implements OnInit {
  public task: Task = new Task();
  public title: string;
  public description: string;

  constructor(
    public navCtrl: NavController,
    public taskService: TaskService,
    ) { }

  ngOnInit() {
  }
  newTask(){
    this.task.title = this.title
    this.task.description = this.description
    
    this.taskService.newTask(this.task)
    this.goBack()
  }

  goBack(){
    this.navCtrl.pop()
  }
}
