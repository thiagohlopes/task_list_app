import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Task } from '../model/task.model';

@Injectable()
export class TaskService {

    private url: string = 'http://localhost:3000/api/task/';
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) { }


    public getAllTask(): Promise<Task[]> {
        return this.http.get(this.url)
            .toPromise()
            .then((resposta: any) => resposta);
    }

    public newTask(Task): Promise<any> {
        return this.http.post(this.url, Task, this.httpOptions).toPromise().then((res) => res);
    }

    public editTask(id, Task): Promise<any> {
        return this.http.put(this.url + id, Task, { responseType: 'text' }).toPromise().then((res) => res);
    }

    public deleteTask(id) {
        return this.http.delete(this.url + id, { responseType: 'text' })
            .toPromise()
            .then((resposta: any) => resposta);
    }
}