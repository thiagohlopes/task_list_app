export class Task {
    public id: number
    public title: string
    public description: string
    public data: string
}